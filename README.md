# IoT-PT

|__IoT Penetration testing Framework__  |
| --------------------------------------|
| 1.[IoTSecFuzz](https://gitlab.com/invuls/iot-projects/iotsecfuzz)			        |
| 2.[Expliot Framework](https://gitlab.com/expliot_framework/expliot)                   |
| 3.[Routersploit](https://github.com/threat9/routersploit)			|


| __Firmware Reverse engineering:__     |
| --------------------------------------|
| 1. [binwalk](https://github.com/ReFirmLabs/binwalk)                            |
| 2. [firmwalker](https://github.com/craigz28/firmwalker)                         |
| 3. [FACT-core](https://github.com/fkie-cad/FACT_core)                          |
| 4. [radare2](https://github.com/radareorg/radare2)                            |
| 5. [capstone](http://www.capstone-engine.org/)                           |
| 6. [angr](https://github.com/angr/angr)                               |
| 7. [flawfinder](https://github.com/david-a-wheeler/flawfinder)                         |
| 8. [firmware modkit](https://github.com/rampageX/firmware-mod-kit)                    |
| 9. [r2ghidra-dec](https://github.com/radareorg/r2ghidra-dec)                       |
 

| __Firmware emulating:__	|
| ------------------------------|
| 1. [FAT tool](https://github.com/attify/firmware-analysis-toolkit)                   |
| 2. [Qemu](https://github.com/qemu/qemu)             		|	
| 3. [Qiling](https://github.com/qilingframework/qiling)         		|
| 4. [Firmadyne](https://github.com/firmadyne/firmadyne)        		|



| __BluetoothTool__ | __Hardware Requirements__ | 
| -------------------|---------------------------|
| [Gattacker](https://github.com/securing/gattacker)         | CSR 4.0                   | 
| [Bluez](http://www.bluez.org/)             | CSR 4.0                   | 
| [bettercap](https://www.bettercap.org/)         | CSR 4.0                   |
| [btlejuice](https://github.com/DigitalSecurity/btlejuice)         | CSR 4.0                   |
| [nrfconnect](https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Connect-for-desktop)        | NRF52840                  |
| [sniffle](https://github.com/nccgroup/Sniffle)           | TI CC1352R                |


	
|__Hardware:__	    |
| ------------------|
| 1.[flashrom](https://flashrom.org/Flashrom)        |
| 2.[openocd](https://github.com/ntfreak/openocd)         |
	
|__Apk Analyzers:__ |
| ------------------|
| 1.[MobSF](https://github.com/MobSF/Mobile-Security-Framework-MobSF)           |
| 2.[QARK](https://github.com/linkedin/qark)            | 
| 3.[Objection](https://github.com/sensepost/objection)       |

